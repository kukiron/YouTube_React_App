# YouTube_React_App

React App with YouTube API

### Getting Started

Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://gitlab.com/kukiron/YouTube_React_App.git
> cd YouTube_React_App
> npm install
> npm start
```

#### Not Familiar with Git?
Download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
> npm install
> npm start
```
